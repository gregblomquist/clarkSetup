#!/bin/bash
##
## Setup script for Anthropological Genetics on tc.rnet.missouri.edu  aka clark
##
##
## Students must log into clark, download this script and execute it:
##     wget https://gitlab.com/gregblomquist/clarkSetup/raw/master/setup.sh -O setup.sh && bash setup.sh
##  or (if you want less typing)
##     wget https://tinyurl.com/yawzbfya -O setup.sh && bash setup.sh
##  or (for even less typing)
##     wget https://bit.ly/2He9tTf -O setup.sh && bash setup.sh
##
## Then, students should log out and back in and tmux auto reconnect should work,
## with the environment variables properly set.
##
## Finally, students have to launch emacs to get it set up properly. The use-package lines
## within the ~/.emacs will automatically install all required packages. Some prompts may
## require answering 'y', e.g. about saving abbrevs.
##
##
echo " grabbing configuration files..."
cd ~
git clone https://gitlab.com/gregblomquist/clarkSetup.git
mv ~/clarkSetup/.dotfiles  ~/.dotfiles
rm -rf ~/clarkSetup

echo " making required directories..."
## make some necessary directories with desired permissions
## emacs backup and autosave to keep directories clean
## -p flag will ignore errors is directory already exists
mkdir -p ~/.tmp/backup
mkdir -p ~/.tmp/autosave
chmod go-rwx ~/.tmp/backup
chmod go-rwx ~/.tmp/autosave
## user's ~/bin  - in case they need other software
## mine gets included in the $PATH from ~/.bash_profile  WON'T WORK!!
##  mkdir -p ~/bin

echo " grabbing genetics software..."
## you must put all the desired software into their ~/bin...
## grab compressed archive that is up on Box.com ~50MB and extract here
wget -O clark-bin.tar.gz https://missouri.box.com/shared/static/tr89fn4ndj7zovgnk8s96ladcxm5emy5.gz
tar -xzf clark-bin.tar.gz
rm clark-bin.tar.gz

echo " grabbing R packages..."
## do the same with ~/R, this one is >200MB
## OR ask the cluster admins to install your desired packages ( better solution?)
wget -O clark-R-dir.tar.gz https://missouri.box.com/shared/static/p1f492vjlc3cpgp1s8fk75pk5o2xblm4.gz
tar -xzf clark-R-dir.tar.gz
rm clark-R-dir.tar.gz


echo " making required links..."
## symlink config files from ~/.dotfiles to required locations in ~/
touch ~/.bashrc && rm ~/.bashrc
touch ~/.bash_profile && rm ~/.bash_profile
touch ~/.emacs && rm ~/.emacs
touch ~/.emacs-cheatsheet.org && rm ~/.emacs-cheatsheet.org
touch ~/.gitignore && rm ~/.gitignore
touch ~/.Rprofile && rm ~/.Rprofile
touch ~/.tmux.conf && rm ~/.tmux.conf
touch ~/bin/repic && rm ~/bin/repic
touch ~/bin/tpic && rm ~/bin/tpic
ln -s ~/.dotfiles/.bashrc ~/.bashrc
ln -s ~/.dotfiles/.bash_profile ~/.bash_profile
ln -s ~/.dotfiles/.emacs ~/.emacs
ln -s ~/.dotfiles/.emacs-cheatsheet.org ~/.emacs-cheatsheet.org
ln -s ~/.dotfiles/.gitignore ~/.gitignore
ln -s ~/.dotfiles/.Rprofile ~/.Rprofile
ln -s ~/.dotfiles/.tmux.conf ~/.tmux.conf
ln -s ~/.dotfiles/hterm-reshow ~/bin/repic
ln -s ~/.dotfiles/hterm-show-file.sh ~/bin/tpic


echo " configuring git..."
## make a ~/.gitconfig from the user's information on clark
## probably not necessary but useful if they learn/know git for version control
git config --global user.name "`whoami` `hostname`"
git config --global user.email "`whoami`@`hostname`"


## finish up
echo "Setup complete! Please log out of clark (i.e. exit) and then reconnect."
