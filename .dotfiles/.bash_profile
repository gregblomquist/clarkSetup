# .bash_profile

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
	. ~/.bashrc
fi

# User specific environment and startup programs
# ~/bin will be populated from compressed download

PATH=$HOME/.local/bin:$HOME/bin:$HOME/bin/phylip-3.697/exe:$PATH

export PATH

# for emacsclient to tolerate starting up with no server initially running
export EDITOR="emacsclient -nw"
export ALTERNATE_EDITOR=""

# load desired system modules
#module load gnutls openssl # assist emacs package downloads with old default emacs 24.3
#module load R              # tc's old default R version 2.3 or so
# newer emacs version sysadmims installed for anthro genetics
module load emacs/emacs-25.3  > .emacs_load.log 2>&1     
# newer R version, send load messages into log files, many worrisome warnings but it works...
module load r/r-3.5.0-python-2.7.14  > .r_load.log 2>&1  
