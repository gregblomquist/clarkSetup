# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions

# set terminal type for rich color
TERM=xterm-256color

# set aliases
alias tmux="tmux -2" # fix color problems if we want tmux to help with attach/detach
# emacs and R modules loaded in .bash_profile 
alias emt="emacsclient -nw" # force non-GUI emacs for speed and connect to server!
alias kill_emacs="emacsclient -e '(kill-emacs)'" # pull the plug on emacs server entirely 
# assume R will only ever be launched from there with ESS or via ~/bin/spin
# both of those methods enforce srun job scheduling


# always connect to a tmux session
## tmux seems to cause problems for Mac users and also gets in the way of emacs-host - copy/paste
## try another solution with emacs as a server aka daemon mode
# kill the tmux server if you want to restart it
#if [ -z "$TMUX" ] && [ -n "$SSH_TTY" ] && [[ $- =~ i ]]; then
#    tmux attach-session -t ssh || tmux new-session -s ssh
#    exit
#fi
