;; configure emacs's built-in package installer configuration
;; requires openssl and gnutls modules to be loaded on teaching cluster (clark)
;; these come as r module dependencies, so you should be fine once r is loaded
(require 'package)
(let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
                    (not (gnutls-available-p))))
       (proto (if no-ssl "http" "https")))
  ;; Comment/uncomment these two lines to enable/disable MELPA and MELPA Stable as desired
  (add-to-list 'package-archives (cons "melpa" (concat proto "://melpa.org/packages/")) t)
;;  (add-to-list 'package-archives (cons "melpa-stable" (concat proto "://stable.melpa.org/packages/")) t)
  (when (< emacs-major-version 24)
    ;; For important compatibility libraries like cl-lib
    (add-to-list 'package-archives '("gnu" . (concat proto "://elpa.gnu.org/packages/")))))
(package-initialize)



;; set up the use-package package for easy package configuration and installation
;; on first launch of emacs use-package will be installed along with configured packages below
(unless (package-installed-p 'use-package)
   (package-refresh-contents)
   (package-install 'use-package))
(require 'use-package)
(eval-when-compile
  (require 'use-package))


;; ivy-counsel-swiper search and completion framework
(use-package ivy
  :ensure t
  :config
  (ivy-mode 1)
  (setq ivy-use-virtual-buffers t
	ivy-count-format "(%d/%d) ")
  )
(use-package counsel
  :ensure t
  :config
  (counsel-mode 1))
(use-package company
  :ensure t
  :hook
  (after-init . global-company-mode)
  :bind
  (:map company-active-map
        ("<tab>" . company-complete-common-or-cycle)))


;; quick buffer changing package modeled on Windows Alt-Tab
;; see https://github.com/killdash9/buffer-flip.el
;; here we bind it to M-n for "next" buffer and M-b for "back"
;; C-x b still works to get list of open buffers by completion list
;; note global binding M-n below also and window switching on M-w
; I think it's working
(use-package buffer-flip
  :ensure t
  :bind  (("M-n" . buffer-flip)
	   :map buffer-flip-map
               ( "M-n" .   buffer-flip-forward) 
               ( "M-b" .   buffer-flip-backward) 
               ( "C-g" . buffer-flip-abort)))


;; search and replacement with counter (n/T) in the modeline
;; I like putting this on M-f for [f]ind and replace or "[m]odi[f]ind"
;; I put C-f as [f]ind with swiper (see below)
(use-package anzu
  :ensure t
  :bind (("M-f" . anzu-query-replace)
         ("C-M-f" . anzu-query-replace-regexp))
  :diminish (anzu-mode . "")
  :init
  (global-anzu-mode +1))


;; emacs speaks statistics - support for R
(use-package ess
   :ensure t
   :defer t
   :init (require 'ess-r-mode) ;; use instead of ess-site to avoid julia, SAS etc.
   :config (setq ess-smart-S-assign-key nil
		 ess-ask-for-ess-directory nil
		 ess-eval-visibly 'nowait
		 ess-use-company t) ;; stop "_" to "<-" conversion and nag on R starting,
   ;; add company integration and don't block editing the source while a long submission is running
   :bind (("M-l" . (lambda () (interactive) (message "Spinning html report, please wait ...") (shell-command (concat "spin " buffer-file-name)) ))
	  ("C-l" . ess-eval-region-or-line-and-step)) ;; C-l = "lines" instead of C-RET
   ;; spin buffer with M-l
   ;; uses script at ~/bin/spin, only create html output with figures and md intermediaries cleaned up
   )
(setq-default inferior-R-program-name "srunR") ;; to use slurm job scheduler, script in ~/bin/srunR
;; https://stackoverflow.com/a/12574794
;; this works but causes annoying echo of code within the console
;; right under the > prompt where it was submitted!
;; fun feature: try M-x ess-rdired to get a list of objects in your R session...

;; color-matching parentheses - very handy in R code
(use-package rainbow-delimiters
  :ensure t
  :config
  (add-hook 'prog-mode-hook 'rainbow-delimiters-mode)
  (add-hook 'ess-mode-hook 'rainbow-delimiters-mode)
  (add-hook 'LaTeX-mode-hook 'rainbow-delimiters-mode)  
  )


;; line fill/unfill M-q toggle instead of 1-way conversion
;; manually set 80 characters as the width for paragraphs
(use-package unfill
  :ensure t
  :bind ([remap fill-paragraph] . unfill-toggle)
  )
(setq-default fill-column 80)


;; configure org mode to be more CUA-compatible
(use-package org
  :config
  (setq org-support-shift-select 'always
	org-replace-disputed-keys t
	org-hide-emphasis-markers t)
  )






;; get common copy/cut/paste bindings = C-c/C-x/C-v
;; https://www.emacswiki.org/emacs/CuaMode
(cua-mode t)
(setq cua-auto-tabify-rectangles nil) ;; Don't tabify after rectangle commands
(transient-mark-mode 1) ;; No region when it is not highlighted
(setq cua-keep-region-after-copy t) ;; Standard Windows behaviour
;; some ideas to ease emacs use for a novice
;; adapted from https://stackoverflow.com/a/2903358
;; and https://www.emacswiki.org/emacs/MsWindowsCustomize
;; Note that C-RET C-TAB and C-S-* will not work well over ssh
;; Also, C-b conflicts with tmux - avoid it, C-m = RET and can't be rebound for ssh/console emacs
;;
;;
;; make easy switching between windows (aka splits), i.e. change which window the point/cursor is in
;; mouse click also works even over ssh (see below), default C-x o still works, too
;; could sorta mimic Rstudio (& others) where each window identified by a numeral, just use 1 as trigger here
;(global-set-key (kbd "C-1") 'other-window)
;; simpler to use "w" for move "window" moving - radical departure from standard emacs copying with M-w
;; could integrate with ace-window eventually for cases of >=3 windows
(global-set-key (kbd "M-w") 'other-window) ;; "w" for moving among visible [w]indows
;;
;; make easy cycling across open buffers, i.e. change the buffer in the current window
;; C-x b still works to get list of open buffers by completion list
(global-set-key (kbd "M-n") 'buffer-flip) ;; package configuration above
;;
;;
;; Search (forward) with C-f  "find", hit RET to move point to found location
(global-set-key (kbd "C-f") 'swiper)
;;(define-key isearch-mode-map (kbd "C-f") 'isearch-repeat-forward)
;; Replace (forward) with M-f, y or n to accept or decline replacement
;;(global-set-key (kbd "M-f") 'query-replace) ;; replaced by anzu above
;;
;;
;; Save with C-s / M-s = save / save as
(global-set-key (kbd "C-s") 'save-buffer)
(global-set-key (kbd "M-s") 'write-file) 
;; need to redefine them for isearch mode (don't know why)
(define-key isearch-mode-map (kbd "C-s") 'save-buffer)
(define-key isearch-mode-map (kbd "M-s") 'write-file) 
;;
;; Close buffer with C-w, (remapping the usual C-x k)
(global-set-key (kbd "C-w") 'kill-this-buffer)
;;
;; delete = delete
(global-set-key [delete] 'delete-char)
;;
;; C-o = open file
(global-set-key (kbd "C-o") 'find-file)
;;
;; C-a = select all DON'T let C-a and C-e be beginning and end of line movement
;;(global-set-key (kbd "C-a") 'mark-whole-buffer)
;; C-z = undo - not needed, part of cua-mode
;;(global-set-key "\C-z" 'undo)
;; C-y = redo
;;
;; Map ESC to cancel incomplete command (standard C-g) but more familiar, triple ESC should work still
;; https://www.emacswiki.org/emacs/CancelingInEmacs
(define-key isearch-mode-map [escape] 'isearch-abort)   ;; isearch
(define-key isearch-mode-map "\e" 'isearch-abort)   ;; \e seems to work better for terminals
(global-set-key [escape] 'keyboard-escape-quit) ;; everywhere else - seems pretty universal, may break in minibuffer
;;
;; Exit/Quit emacs = C-q  (remapping the usual C-x C-c)
;; will still prompt to save any unsaved files
(global-set-key (kbd "C-q") 'save-buffers-kill-terminal)


;; some move visually sensible splitting commands - don't work on MobaXterm, do in gnome-terminal
;; (global-set-key (kbd "C-x ~") 'split-window-below) ;; same as C-x 2...
;; (global-set-key (kbd "C-x |") 'split-window-right) ;; and C-x 3, both still usable


;; Make emacs more mouse-friendly in console/terminal 
;; enable mouse clicking to move the point/cursor
;; scrolling the active buffer and resizing windows
(require 'xt-mouse)
(xterm-mouse-mode)
(require 'mouse)
(xterm-mouse-mode t)
(defun track-mouse (e))

(setq mouse-wheel-follow-mouse 't)

(defvar alternating-scroll-down-next t)
(defvar alternating-scroll-up-next t)

(defun alternating-scroll-down-line ()
  (interactive "@")
    (when alternating-scroll-down-next
;      (run-hook-with-args 'window-scroll-functions )
      (scroll-down-line))
    (setq alternating-scroll-down-next (not alternating-scroll-down-next)))

(defun alternating-scroll-up-line ()
  (interactive "@")
    (when alternating-scroll-up-next
;      (run-hook-with-args 'window-scroll-functions)
      (scroll-up-line))
    (setq alternating-scroll-up-next (not alternating-scroll-up-next)))

;; use the mouse scroll wheel to scroll in a buffer
(global-set-key (kbd "<mouse-4>") 'alternating-scroll-down-line)
(global-set-key (kbd "<mouse-5>") 'alternating-scroll-up-line)
;; matching calls for touchpad movement
(global-set-key (kbd "<mouse-36>") 'alternating-scroll-down-line)
(global-set-key (kbd "<mouse-37>") 'alternating-scroll-up-line)


;; simple solution for improved clipboard functionality
;; it requires X forwarding which is already required for viewing R plots
;; pasting *into* emacs even without this
;;    on MobaXterm do right click menu > Paste
;;    on gnome-terminal do Shift-Ctrl+v or Edit menu > Paste
;; this is an attempt to be able to copy *out* of ssh console emacs on clark
;; to local programs like a web browser or word processor
;; with the xclip package here and xclip installed on clark
;; (from git clone, bootstrap configure make, and then move to ~/bin)
;; I can C-c in emacs on clark to copy code
;;    then paste into my Windows Notepad - MobaXterm handles the X-clipboard sharing
;;    then paste into Gedit one cebeulla, X11 for Gnome handles the X-clipboard sharing
;; there's a small delay pasting within emacs (perhaps just with MobaXterm) created by this, but it works!
(use-package xclip
  :ensure t
  :config (xclip-mode 1)
  )

;; quick attempt to fix clipboard in emacs - doesn't work...
;; MobaXterm and gnome-terminal support pasting *into* emacs even without this
;; this is an attempt to be able to copy *out* of ssh console emacs on clark
;; to local programs like a web browser or word processor
;;(setq x-select-enable-clipboard t)
;;
;; more detailed possibile solutions at
;; https://stackoverflow.com/questions/1152362/how-to-send-data-to-local-clipboard-from-a-remote-ssh-session
;; some other ideas at https://unix.stackexchange.com/questions/72605/emacs-copy-and-paste
;; this function when called should write selected text to a specified file 
;; (defun write-to-file-clipboard ()
;;   "Write the region to `~/.fclipboard.txt'"
;;   (interactive)
;;   (write-region (region-beginning) (region-end) "~/.fclipboard.txt")
;;   (shell-command "cat ~/.fclipboard.txt | xclip &")
;;   )
;; Now you just need to automate this being passed via X-forwarding to the local clipboard...
;; if xclip is installed on cebuella I can do line below in a gnome-terminal tab and get the contents in my local clipboard
;; ssh -X -C blomquistg@clark.rnet.missouri.edu "cat ~/.fclipboard.txt" | xclip -selection c
;; note that you can cat this file back at the command line in MobaXterm and then copying works fine
;; of course you can double-click on it in the sftp file brower and look at it in the MobaEditor, too
;; with xclip installed (from git clone, compile and then move to ~/bin) on clark I can do
;;  cat ~/.fclipboard.txt | xclip
;; and the file contents end up in my local clipboard with MobaXterm


;; change some default emacs behavior to beautify interface and clean up temporary files
;; safely suppress menu bar and toolbar for GUI and console emacs
(menu-bar-mode -1)
(if window-system
    (tool-bar-mode -1)
  )
;; get rid of ringing bells
(setq ring-bell-function 'ignore)
;; dark, fruity color scheme that looks nice console or GUI
(use-package monokai-theme
  :ensure t)
;; a light color theme that is also good CLI/GUI, has less aggressive styling
;; (use-package github-modern-theme
;;   :ensure t)
;; turn on line numbering on left side of screen
(global-linum-mode)
(setq linum-format "%4d \u2502")
;; function to turn it off if desired for a select buffer
(defun no-linum ()
  (linum-mode -1)
  )
;; highlight the current line so as not to lose track of the point
(global-hl-line-mode +1)
;; turn of splash screen and make scratch buffer have a more useful message
(setq inhibit-splash-screen t)
;;(setq initial-buffer-choice "~/.emacs-cheatsheet.org")
(setq initial-scratch-message "\
;;
;;        W e l c o m e   t o   e m a c s !
;;
;;
;; Press C-o to open a file.
;;         \u21b3 Ctrl-o (Windows) or Control-o (Mac) 
;;
;; Press M-n to switch among open buffers ≈ files.
;;         \u21b3  Alt-n (Windows) or Alt/Option-n (Mac) 
;;
;; Try  C-o .emacs-cheatsheet.org  for a list of more commands.
;;
;;
;;                                  C-q will quit emacs  \u03A9
;;")
;;
;; usually this is what I want for more familiar line wrapping
;; can toggle off with M-x visual-line-mode at any time
(global-visual-line-mode 1) 


;; decluttered modeline adapted from
;; https://swsnr.de/blog/2014/07/26/make-your-emacs-mode-line-more-useful/
;; could use nyan-cat for progress but text is simpler
(setq-default mode-line-format
              '("%e" mode-line-front-space
                ;; Standard info about the current buffer
;;                mode-line-mule-info
                mode-line-client
                mode-line-modified
                mode-line-remote " " 
;;                mode-line-frame-identification
                mode-line-buffer-identification " "
;;		(:eval (list (nyan-create)))  
		mode-line-position
                ;; Misc information, notably battery state and function name
                " "
                mode-line-misc-info
                ;; modes, minor-modes all suppressed by code below
                " " mode-line-modes mode-line-end-spaces))

;; some additional mode-line tweaks to declutter it
;; kill off all the minor mode displays, https://emacs.stackexchange.com/a/41135
(setq mode-line-modes
      (mapcar (lambda (elem)
                (pcase elem
                  (`(:propertize (,_ minor-mode-alist . ,_) . ,_)
                   "")
                  (t elem)))
              mode-line-modes))
;; kill off line number in modeline - redundant with numbering on left side
;; column number reporting is off by default and slows editing (a little)
(setq line-number-mode nil)


;; additional visual cue to know which buffer has focus
;; looks horrendous and I couldn't get colors customized right with monokai to look nice
;; (use-package auto-dim-other-buffers
;;   :ensure t
;;   :init (auto-dim-other-buffers-mode))

;; could be useful compared to the x% through buffer
;; it works and isn't too ugly on console, not clickable to move, though
;; (use-package nyan-mode
;;   :ensure t
;;   :config (setq nyan-animate-nyancat nil
;; 		nyan-wavy-trail nil
;; 		nyan-bar-length 16
;; 		nyan-cat-face-number 0
;; 		nyan-minimum-window-width 80)
;;   )





;;; backup/autosave:  https://stackoverflow.com/a/2020954
;; manually create ~/.tmp
(defvar backup-dir (expand-file-name "~/.tmp/backup/"))
(defvar autosave-dir (expand-file-name "~/.tmp/autosave/"))
(setq backup-directory-alist (list (cons ".*" backup-dir)))
(setq auto-save-list-file-prefix autosave-dir)
(setq auto-save-file-name-transforms `((".*" ,autosave-dir t)))





