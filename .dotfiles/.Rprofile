## ~/.Rprofile
##
## set package repository so you never have to select one
##
## `local` creates a new, empty environment
## This avoids polluting .GlobalEnv with the object r
local({
  r = getOption("repos")             
  r["CRAN"] = "https://cran.rstudio.com/"
  options(repos = r)
})

## Redefine q() to never save workspace
## Same as saying q("no")
q <- function(save="no", ...) {
  quit(save=save, ...)
}


## Add blomquistg's R packages to .libPaths()   DOES NOT WORK!!
## ~/R can contain other user-installed packages
## system-wide packages and blomquistg's are also available
## .libPaths("/home/blomquistg/R/x86_64-pc-linux-gnu-library/3.5")

## enable piping images to disk for showing in a capable terminal window
## such as hterm (Chrome ssh extension) or iTerm2 on Mac
## dplyr for general piping and data wrangling
## R.devices for toPNG() below
packs <- c("dplyr","R.devices")
invisible(lapply(packs, function(x) require(x, character.only = TRUE, quietly = TRUE)))

## the function below will give a piping target for plots
## it will be ignored when spinning/knitting reports and the
## default behavior of embedding as base64 strings will take over
pipic <- function(expr, img="Rplot"){
    if (isTRUE(getOption('knitr.in.progress'))==FALSE && interactive()){
        ## when NOT knitting/spinning or running Rscript or R CMD etc
        ## default naming that gets overwritten is good
        toPNG(name = img, { capture.output( print(expr) ) } )
    }
    else if (isTRUE(getOption('knitr.in.progress'))){
        ## when knitting/spinning which takes care of file naming
        ## and embedding in html output
        expr
    }
    else if (interactive()==FALSE){
        ## when using Rscript or R CMD etc for text output
        ## should explicitly name files with img argument
        toPNG(name = img, { capture.output( print(expr) ) } )
    }
}
## example usage:
##   library(lattice); data(iris)
##   densityplot( iris$Sepal.Length, xlab="Sepal Length") %>% pipic()
## file Rplot.png will be written to disk in working directory
## in another terminal run the hterm-reshow script and it will
## automatically update the display of the new plot image
## so if you do
##   pipic( {hist(iris$Sepal.Length); rug(iris$Sepal.Length)}  )
## Rplot.png gets overwritten and the new plot gets displayed
## in the terminal


## restrict big dataset prints and decorate command prompt
.First <- function() {
  options(max.print=200)          # do not print more than 200 lines
  options(prompt="R> ", digits=4)
}
